package engine.interfaces;

/**
 * Handles the setup and teardown between munues and levels
*/


interface IState 
{
	public function setup() : Void;
	public function teardown() : Void;
}