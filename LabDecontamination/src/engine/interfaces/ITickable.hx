package engine.interfaces;

/**
 * Handles classes and functions that requires a tick to operate 
*/


interface ITickable 
{
	function tick() : Void;
}


