package engine;

import engine.interfaces.IState;

/**
 * Handles adding and entering states between menues and levels
*/


class StateMachine 
{
	public var activeState:IState;
	public var states:Hash<IState>;

	public function new() : Void
	{
		activeState = null;
		states = new Hash<IState>();
	}
	
	public function addState( name:String, state:IState, isEntryState:Bool = false ) : Void
	{
		states.set( name, state ); 									//putting keys and values into states: associates keys with values
		
		if ( isEntryState )
		{
			enterState( name );
		}
	}
	
	public function enterState( name:String ) : Void
	{
		if ( states.exists( name ) == false )
		{
			trace( "State does not exist: " + name );
			return;
		}
		
		if ( activeState != null )
		{
			activeState.teardown();
		}
		
		activeState = states.get( name );
		activeState.setup();
	}
}