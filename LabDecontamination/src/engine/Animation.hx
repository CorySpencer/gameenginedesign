package engine;

import engine.interfaces.ITickable;

import nme.display.Sprite;
import nme.display.Bitmap;

/**
 * Allows sprites to swap throuh animations
*/


class Animation extends Sprite, implements ITickable
{
	public var frames:Array<Bitmap>;
	public var currentFrame:Int;
	public var secondsPerFrame:Float;
	
	private var isActive:Bool;
	private var bmp:Bitmap;
	private var gameFramerate:Float;
	private var timeAggregated:Float;
	private var iterator:Iterator<Bitmap>;
	
	public function new( frames:Array<Bitmap>, playRate:Float, gameFramerate:Float ) : Void
	{
		super();
		iterator = frames.iterator();
		while ( iterator.hasNext() )
		{
			bmp = iterator.next();
			bmp.visible = false;
			this.addChild( bmp );
			bmp.x -= bmp.width / 2;
			bmp.y -= bmp.height / 2;
		}
		
		this.frames = frames;
		isActive = false;
		currentFrame = 0;
		this.secondsPerFrame = 1.0 / playRate;
		this.gameFramerate = 1.0 / gameFramerate;
	}
	
	public function start() : Void
	{
		currentFrame = 0;
		frames[0].visible = true;
		isActive = true;
		timeAggregated = 0.0;
	}
	
	public function stop() : Void
	{
		isActive = false;
		frames[currentFrame].visible = false;
	}
	
	
	public function tick() : Void
	{
		if ( isActive )
		{
			if ( ( timeAggregated += gameFramerate ) >= secondsPerFrame )
			{
				timeAggregated = 0.0;
				frames[currentFrame].visible = false;
				currentFrame = ( currentFrame == frames.length - 1 ) ? 0 : currentFrame + 1;
				frames[currentFrame].visible = true;  
			}
		}
	}
}