package engine;

import engine.Rectangle;
import engine.render.Actor;
import game.TouchButtons;

import nme.display.Stage;
import nme.display.DisplayObject;
import nme.display.Sprite;

/**
 * Handles where assests are loading into the stage
 * via background layer, game layer, foreground layer
*/


class SceneManager 
{
	public var backgroundLayer:Sprite;
	public var gameLayer:Sprite;
	public var foregroundLayer:Sprite;	
	public var buttons:TouchButtons;
	
	private var currentLevel:Int = 0;
	private var highestLevelWon:Int = -1;
	private var layer:Int;
	private var other:Actor;
	private var arrayOfIntersectingActors:Array<Actor>;
	
	public function new( stage:Stage ) : Void
	{	
		backgroundLayer = new Sprite();
		gameLayer = new Sprite();
		foregroundLayer = new Sprite();
		
		stage.addChildAt( backgroundLayer, 0 );
		stage.addChildAt( gameLayer, 1 );
		stage.addChildAt( foregroundLayer, 2 );
	}
	
	public function setBackground( child:DisplayObject ) : Void
	{
		while ( backgroundLayer.numChildren > 0 )
		{
			backgroundLayer.removeChildAt( 0 );
		}
		
		backgroundLayer.addChild( child );
	}
	
	public function addToGameLayer( child:DisplayObject ) : Void
	{
		gameLayer.addChild( child );
	}
	
	public function addToForeground( child:DisplayObject ) : Void
	{
		foregroundLayer.addChild( child );
	}
	
	public function removeFromGameLayer( child:DisplayObject ) : Void
	{
		if ( gameLayer.contains( child ) )
		{
			gameLayer.removeChild( child );
		}
	}
	
	public function removeFromForeground( child:DisplayObject ) : Void
	{
		foregroundLayer.removeChild( child );
	}
	
	public function rectSceneQuery( rect:Rectangle ) : Array<Actor>
	{
		arrayOfIntersectingActors = new Array<Actor>(); 								//new blank array
		if ( rect == null )
		{
			return( arrayOfIntersectingActors ); 										//returns blank array
		}
		
		layer = gameLayer.numChildren - 1; 												// set as "-1" to avoid out of bounds error
		while ( layer >= 0 )
		{
			other = cast( gameLayer.getChildAt( layer ), Actor ); 						// iterating over children in layer
			if ( rect.intersects( other.actorBoundingBox ) == true )
			{
				arrayOfIntersectingActors.push( other );
			}
			layer--;
		}
		return arrayOfIntersectingActors;
	}
}