package engine.render;

import nme.display.Bitmap;
import nme.display.Stage;
import nme.Assets;


class ActorFactory 
{
	public var sceneMgr:SceneManager;
	public var actor:Actor;

	public function new( sceneMgr:SceneManager ) : Void
	{
		this.sceneMgr = sceneMgr;
	}

	/**
	* Creats an Actor given the Sring, x,y position, and optional collision type
	* @param	drawable from String
	* @param	xPosition floating point x-position.
	* @param	yPosition floating point y-position.
	* @param	collisionType of actor (i.e. HAZARD||OBSTACLE||PICKUP||PORTAL_UP||PORTAL_DOWN) 
	*/
	
	public function getActorFromStringForGameLayer( imgLocation:String, positionX:Float, positionY:Float, collisionType:String = "" ) : Actor
	{
		actor = new Actor( new Bitmap( Assets.getBitmapData( imgLocation ) ) );
		sceneMgr.addToGameLayer( actor );
		actor.setActorPos( positionX, positionY );
		actor.collisionType = collisionType;
		return actor;	
	}
	
	public function getActorFromStringForForeground( imgLocation:String, positionX:Float, positionY:Float, collisionType:String = "" ) : Actor
	{
		actor = new Actor( new Bitmap( Assets.getBitmapData( imgLocation ) ) );
		sceneMgr.addToForeground( actor );
		actor.setActorPos( positionX, positionY );
		actor.collisionType = collisionType;
		return actor;
	}
}