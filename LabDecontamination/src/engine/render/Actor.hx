package engine.render;

import engine.Rectangle;

import nme.Lib;
import nme.Assets;
import nme.display .Bitmap;
import nme.display.Sprite;
import nme.display.Stage;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.events.TouchEvent;
import nme.geom.Point;


class Actor extends Sprite
{
	public static inline var PICKUP:String = "PICKUP";					//collision type: access card handler
	public static inline var HAZARD:String = "HAZARD";					//collision type:chemical handler
	public static inline var OBSTACLE:String = "OBSTACLE";				//collision type:table handler
	public static inline var PORTAL_UP:String = "PORTAL_UP";			//collision type:access previous floor handler
	public static inline var PORTAL_DOWN:String = "PORTAL_DOWN";		//collision type:access next floor handler
	
	public var collisionType:String;
	public var actorBoundingBox:Rectangle;
	public var actorSprite:Sprite;
	public var currentAnimation:Animation;
	
	private var actorBitmap:Bitmap;
	private var actorHalfWidth:Float;
	private var actorHalfHeight:Float;
	private var animations:Hash<Animation>;
	
	public function new( bmp:Bitmap ) : Void
	{
		super ();
		collisionType = "";
		actorBitmap = bmp;
		
		if ( actorBitmap != null )
		{
			actorBoundingBox = new Rectangle( 0.0, 0.0, actorBitmap.width, actorBitmap.height );
		}
		else
		{
			actorBoundingBox = new Rectangle( 0.0, 0.0, 38.0, 51.0 );
		}
		
		addEventListener( Event.ADDED_TO_STAGE, drawActor );
		currentAnimation = null;
		animations = null;
	}
	
	public function addAnimation( name:String, animation:Animation ) : Void
	{
		if ( animations == null )
		{
			animations = new Hash<Animation>();
		}
		
		if ( !animations.exists( name ) )
		{
			animations.set( name, animation );
			this.addChild( animation );
		}
		else
		{
			trace( "Already have an animation of the name: " + name );
		}
	}
	
	public function runAnimation( name:String ):Void
	{
		if ( ( animations != null ) && ( animations.exists( name ) ) )
		{
			if ( currentAnimation != null )
			{
				currentAnimation.stop();
			}
			
			currentAnimation = animations.get( name );
			currentAnimation.start();
		}
		else
		{
			trace( "Requested nonexistant animation: " + name );
		}
	}
	
	public function drawActor( event:Event ) : Void
	{	
		if ( actorBitmap != null )
		{
			actorSprite = new Sprite();
			actorSprite.addChild( actorBitmap );
			addChild( actorSprite );
			actorSprite.x -= actorSprite.width / 2.0;
			actorSprite.y -= actorSprite.height / 2.0;
		}
		
		removeEventListener( Event.ADDED_TO_STAGE, drawActor );
	}
	
	public function getActorPos():Point
	{
		var centerPoint = new Point( this.x, this.y );
		return centerPoint;
	}
	
	public function setActorPos( x:Float, y:Float ) : Void						//Takes position of the actor and boundingBox and moves them together
	{
		this.x = x;
		this.y = y;
		actorBoundingBox.setTo( this.x - ( this.width / 2.0 ), this.y - ( this.height / 2.0 ), actorBoundingBox.boxWidth, actorBoundingBox.boxHeight );
	}
}
