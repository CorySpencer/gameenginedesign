package engine;

import nme.geom.Point;


class Rectangle 
{
	public var x:Float;
	public var y:Float;
	public var boxHeight:Float;
	public var boxWidth:Float;
	
	/**
	* Creats an Rectangle given the x,y position, width and height
	* @param	xPosition floating point x-position.
	* @param	yPosition floating point y-position.
	* @param	width of rectangle 
	* @param	height of rectangle 
	*/
	
	public function new( x:Float, y:Float, width:Float, height:Float ) : Void
	{
		setTo( x, y, width, height );
	}
	
	public function setTo( x:Float, y:Float, width:Float, height:Float ) : Void
	{
		this.x = x;
		this.y = y;
		boxHeight = height;
		boxWidth = width;
	}
	
	public function intersects( other:Rectangle ) : Bool
	{
		//return !(x_1 > x_2+width_2 || x_1+width_1 < x_2 || y_1 > y_2+height_2 || y_1+height_1 < y_2); --> //http://wiki.processing.org/w/Rect-Rect_intersection
		return !( this.x > other.x + other.boxWidth || this.x + this.boxWidth < other.x || this.y > other.y + other.boxHeight || this.y + this.boxHeight < other.y );
	}
}