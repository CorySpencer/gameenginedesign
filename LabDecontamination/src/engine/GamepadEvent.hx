package engine;

import nme.events.KeyboardEvent;

/**
 * Handles the gamepad and sets it identical to keyboard controls
*/


class GamepadEvent extends KeyboardEvent
{
	public var direction:String;
	
	public function new( type:String, dir:String ) : Void
	{
		super( type );
		direction = dir;
	}
}