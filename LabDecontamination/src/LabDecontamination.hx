package ;

import engine.render.Actor;
import engine.render.ActorFactory;
import engine.Animation;
import engine.SceneManager;
import engine.StateMachine;
import engine.Rectangle;
import game.states.TitleScreenState;
import game.states.InstructionScreenState;
import game.PlayerCollision;
import game.PlayerController;
import game.LevelLayout;
import game.TouchButtons;
import game.MenuNavEvent;

import nme.Lib;
import nme.Assets;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.display.Stage;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.media.Sound;
import nme.media.SoundChannel;
import nme.text.Font;
import nme.text.TextFormat;
import nme.text.TextField;


class LabDecontamination extends Sprite
{   
	public var player:Actor;
    public var background:Bitmap;
	public var controller:PlayerController;
	public var collisions:PlayerCollision;
	public var sceneMgr:SceneManager;
	public var factory:ActorFactory;
	public var scenario:StateMachine;
	public var level:LevelLayout;
	public var touchButtons:TouchButtons;
	
	private var menuMachine:StateMachine = null;
	private var soundTitle:Sound;
	private var theFont:Font;
	private var timerField:TextField;
	private var titleScreen:TitleScreenState;
	private var instructionScreen:InstructionScreenState;
	private var timerText:TextFormat;
	private var bg_music:Sound;
	private var explodeAnimation:Animation;
	private var explode:Array<Bitmap>;
	private var idleUp:Array<Bitmap>;
	private var idleDown:Array<Bitmap>;
	private var idleLeft:Array<Bitmap>;
	private var idleRight:Array<Bitmap>;
	private var up:Array<Bitmap>;
	private var down:Array<Bitmap>;
	private var left:Array<Bitmap>;
	private var right:Array<Bitmap>;
	private var idleUpStay:Animation;
	private var idleDownStay:Animation;
	private var idleLeftStay:Animation;
	private var idleRightStay:Animation;
	private var walkUp:Animation;
	private var walkDown:Animation;
	private var walkLeft:Animation;
	private var walkRight:Animation; 
	private var timeToExplode:Float = 5.0;
	private var timeToShowAndrew:Float = 5.0;
	private var andrew:Bitmap = null;
	private var bg_musicChannel:SoundChannel;
	private var timeRemaining:Float = MAX_GAME_TIME;
	private static inline var MAX_GAME_TIME:Float = 600.0;
	
	
    public function new() : Void
    {
        super ();	
		addEventListener( Event.ADDED_TO_STAGE, makeMenuMachine );
		bg_music = Assets.getSound("assets/audio/bg_game_music.mp3");
    }
	
	public function makeMenuMachine( event:Event ) : Void
	{
		if ( menuMachine == null )
		{
			menuMachine = new StateMachine();
			
			titleScreen = new TitleScreenState( stage );
			titleScreen.addEventListener( MenuNavEvent.START_TYPE, navEventHandler );
			titleScreen.addEventListener( MenuNavEvent.INSTRUCTION_TYPE, navEventHandler );
			
			instructionScreen = new InstructionScreenState( stage );
			instructionScreen.addEventListener( MenuNavEvent.TITLE_TYPE, navEventHandler );
			
			menuMachine.addState("titleScreen", titleScreen, true);
			menuMachine.addState("instruction", instructionScreen);
		}
	}
	
	public function navEventHandler( event:MenuNavEvent ):  Void
	{
		if ( event.type == MenuNavEvent.START_TYPE )
		{
			titleScreen.removeEventListener( MenuNavEvent.START_TYPE, navEventHandler );
			instructionScreen.removeEventListener( MenuNavEvent.TITLE_TYPE, navEventHandler );
			titleScreen.removeEventListener( MenuNavEvent.INSTRUCTION_TYPE, navEventHandler );
			
			menuMachine.activeState.teardown();
			onGameScreen();
		} 
		else if ( event.type == MenuNavEvent.INSTRUCTION_TYPE )
		{
			menuMachine.enterState("instruction");
		} 
		else if ( event.type == MenuNavEvent.TITLE_TYPE )
		{
			menuMachine.enterState("titleScreen");
		}
	}
	
    public function onGameScreen( ) : Void
    {
		soundTitle = Assets.getSound( "assets/audio/labDecontaminationInitiated.mp3" );
		soundTitle.play();
		
		bg_musicChannel = bg_music.play(5000.0, 100);
		
		sceneMgr = new SceneManager( stage );
		background = new Bitmap( Assets.getBitmapData ( "assets/img/levelAssets/floor.png" ) );
		sceneMgr.setBackground( background );
		
		factory = new ActorFactory( sceneMgr );
		this.player = new Actor( null );
		sceneMgr.addToForeground( player );
		buildPlayerAnimations();		
		
		scenario = new StateMachine();
		
		level = new LevelLayout( sceneMgr, factory );
		level.initB0Level();
		scenario.addState( "BLevel0", level, true);
		this.player.setActorPos( level.playerPosition.x, level.playerPosition.y );
		
		level = new LevelLayout( sceneMgr, factory );
		level.initB1Level();
		scenario.addState( "BLevel1", level);
		
		level = new LevelLayout( sceneMgr, factory );
		level.initB2Level();
		scenario.addState( "BLevel2", level );
		
		level = new LevelLayout( sceneMgr, factory );
		level.initB3Level();
		scenario.addState( "BLevel3", level );
		
		level = new LevelLayout( sceneMgr, factory );
		level.initB4Level();
		scenario.addState( "BLevel4", level );
		
		level = new LevelLayout( sceneMgr, factory );
		level.initB5Level();
		scenario.addState( "BLevel5", level );
		
		level = new LevelLayout( sceneMgr, factory );
		level.initB6Level();
		scenario.addState( "BLevel6", level );
		
		level = new LevelLayout( sceneMgr, factory );
		level.initB7Level();
		scenario.addState( "BLevel7", level );
		
		level = new LevelLayout( sceneMgr, factory );
		level.initB8Level();
		scenario.addState( "BLevel8", level );
		
		touchButtons = new TouchButtons( sceneMgr, factory );
		sceneMgr.buttons = touchButtons;
		controller = new PlayerController( player, this.stage, sceneMgr, scenario );
		
		addEventListener( Event.ENTER_FRAME, tick );
		
        timerField = new TextField();
		stage.addChild( timerField );
		
		explode = new Array<Bitmap>();
		explode.push( new Bitmap( Assets.getBitmapData ( "assets/img/explodeAnimations/explodeAnimation1.png" ) ) );
		explode.push( new Bitmap( Assets.getBitmapData ( "assets/img/explodeAnimations/explodeAnimation2.png" ) ) );
		explode.push( new Bitmap( Assets.getBitmapData ( "assets/img/explodeAnimations/explodeAnimation3.png" ) ) );
		explode.push( new Bitmap( Assets.getBitmapData ( "assets/img/explodeAnimations/explodeAnimation4.png" ) ) );
		explode.push( new Bitmap( Assets.getBitmapData ( "assets/img/explodeAnimations/explodeAnimation5.png" ) ) );
		explodeAnimation = new Animation( explode, 30.00, stage.frameRate );
		stage.addChild(explodeAnimation);
		explodeAnimation.x += explodeAnimation.width / 2.0;
		explodeAnimation.y += explodeAnimation.height / 2.0;
		
		//var secondsPerFrame:Float = 1.0 / stage.frameRate;
    }
		
	public function showGameOver() : Void
	{
		if ( timeToExplode == 5.0 )
		{
			explodeAnimation.start();
		}
		
		if ( timeToExplode > 0.0 )
		{
			explodeAnimation.tick();
		}
		
		if ( (timeToExplode -= ( 1.0 / stage.frameRate ) ) <= 0.0 )
		{
			explodeAnimation.stop();
			
			if ( andrew == null )
			{
				andrew = new Bitmap( Assets.getBitmapData ( "assets/img/endScreen.png" ) );
				stage.addChild( andrew );
			}
			
			if ( (timeToShowAndrew -= ( 1.0 / stage.frameRate ) ) <= 0.0 )
			{
				removeEventListener( Event.ENTER_FRAME, tick );
				timeToExplode = 5.0;
				scenario = null;
				menuMachine = null;
				timeRemaining = MAX_GAME_TIME;
				timeToShowAndrew = 5.0;
				andrew = null;
				bg_musicChannel.stop();
				bg_musicChannel = null;
				
				while ( stage.numChildren > 1 )
				{
					if ( stage.getChildAt(0) == this )
					{
						stage.removeChildAt(1);
					}
					else
					{
						stage.removeChildAt(0);
					}
				}
				makeMenuMachine( null );
			}
		}
	}
	
	private function timeElasped() : Void
	{
		timerText = new TextFormat();
		timerText.font = "Arial";
        timerText.size = 30;
        timerText.color=0xFF0000;
        timerField.text = "Time Remaining: " + Math.ceil(timeRemaining);
        timerField.setTextFormat(timerText);
		timerField.width = stage.width;
		timerField.x = 775;
	}
	
	private function buildPlayerAnimations() : Void
	{
		idleUp = new Array<Bitmap>();
		idleUp.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_idle_up.png" ) ) );
		idleUpStay = new Animation( idleUp, 3.00, stage.frameRate );
		player.addAnimation( "idleUp", idleUpStay );
		player.runAnimation( "idleUp" );
		
		idleDown = new Array<Bitmap>();
		idleDown.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_idle_down.png" ) ) );
		idleDownStay = new Animation( idleDown, 3.00, stage.frameRate );
		player.addAnimation( "idleDown", idleDownStay );
		player.runAnimation( "idleDown" );
		
		idleLeft = new Array<Bitmap>();
		idleLeft.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_idle_left.png" ) ) );
		idleLeftStay = new Animation( idleLeft, 3.00, stage.frameRate );
		player.addAnimation( "idleLeft", idleLeftStay );
		player.runAnimation( "idleLeft" );
		
		idleRight = new Array<Bitmap>();
		idleRight.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_idle_right.png" ) ) );
		idleRightStay = new Animation( idleRight, 3.00, stage.frameRate );
		player.addAnimation( "idleRight", idleRightStay );
		player.runAnimation( "idleRight" );
		
		up = new Array<Bitmap>();
		up.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_walk_up1.png" ) ) );
		up.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_walk_up2.png" ) ) );
		walkUp = new Animation( up, 3.00, stage.frameRate );
		player.addAnimation( "walkUp", walkUp );
		player.runAnimation( "walkUp" );
		
		down = new Array<Bitmap>();
		down.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_walk_down1.png" ) ) );
		down.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_walk_down2.png" ) ) );
		walkDown = new Animation( down, 3.00, stage.frameRate );
		player.addAnimation( "walkDown", walkDown );
		player.runAnimation( "walkDown" );
		
		left = new Array<Bitmap>();
		left.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_walk_left1.png" ) ) );
		left.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_walk_left2.png" ) ) );
		walkLeft = new Animation( left, 3.00, stage.frameRate );
		player.addAnimation( "walkLeft", walkLeft );
		player.runAnimation( "walkLeft" );
		
		right = new Array<Bitmap>();
		right.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_walk_right1.png" ) ) );
		right.push( new Bitmap( Assets.getBitmapData ( "assets/img/playerAnimations/player_walk_right2.png" ) ) );
		walkRight = new Animation( right, 3.00, stage.frameRate );
		player.addAnimation( "walkRight", walkRight );
		player.runAnimation( "walkRight" );
	}

	public function tick( event:Event ) : Void
	{
		if ( ( timeRemaining -= ( 1.0 / stage.frameRate)) > 0.0 )
		{
			controller.tick();
		}
		else
		{
			timeRemaining = 0.0;
			showGameOver();
		}
		timeElasped();
	}
	
    public static function main () 
    {
        Lib.stage.addChild( new LabDecontamination() );
    }    
}