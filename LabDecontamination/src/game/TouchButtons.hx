package game;

import engine.render.Actor;
import engine.render.ActorFactory;
import engine.interfaces.IState;
import engine.GamepadEvent;
import engine.SceneManager;

import nme.display.Stage;
import nme.display.Sprite;
import nme.events.EventDispatcher;
import nme.events.KeyboardEvent;
import nme.events.Event;
import nme.events.MouseEvent;

/**
 * Handles controller for iOS
*/


class TouchButtons extends EventDispatcher
{
	private var gamepad:Actor;
    private var touchButtonUp:Actor;
    private var touchButtonDown:Actor;
    private var touchButtonLeft:Actor;
    private var touchButtonRight:Actor;
    private var sceneMgr:SceneManager;
    private var factory:ActorFactory;
 
    public function new( sceneMgr:SceneManager, factory:ActorFactory) : Void
    {
        super();
        this.sceneMgr = sceneMgr;
        this.factory = factory;
        
		#if iphone
        gamepadLayout();
        upButton();
        downButton();
        leftButton();
        rightButton();
		#end
    }

	public function gamepadLayout() : Void
	{
		gamepad = factory.getActorFromStringForForeground( "assets/img/buttons/controlpad_layout.png", 112.00, 517.50 );
	}

    public function upButton() : Void 
	{   
		touchButtonUp = factory.getActorFromStringForForeground( "assets/img/buttons/button_up_down.png", 112.50, 434.00 );
		touchButtonUp.buttonMode = true;
	   
		touchButtonUp.addEventListener( MouseEvent.MOUSE_DOWN, function( event:Event ) 
		{
			dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_DOWN, "w" ) );
		} );
       
		touchButtonUp.addEventListener( MouseEvent.MOUSE_UP, function( event:Event ) 
		{
		   dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_UP, "w" ) );
		} );
	   
		touchButtonUp.addEventListener( MouseEvent.MOUSE_OUT, function( event:Event ) 
		{
		   dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_UP, "w" ) );
		} );
    }

    public function downButton() : Void 
    {
        touchButtonDown = factory.getActorFromStringForForeground( "assets/img/buttons/button_up_down.png", 112.00, 602.50 );
        touchButtonDown.buttonMode = true;
        
        touchButtonDown.addEventListener( MouseEvent.MOUSE_DOWN, function( event:Event ) 
        {
            dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_DOWN, "s" ) );
        } );
      
        touchButtonDown.addEventListener( MouseEvent.MOUSE_UP, function( event:Event ) 
        {
            dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_UP, "s" ) );
        } );
				
		touchButtonDown.addEventListener( MouseEvent.MOUSE_OUT, function( event:Event ) 
        {
            dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_UP, "s" ) );
        } );
    }
   
	public function leftButton() : Void 
    {
        touchButtonLeft = factory.getActorFromStringForForeground( "assets/img/buttons/button_left_right.png", 38.50, 515.50 );
        touchButtonLeft.buttonMode = true;
        
        touchButtonLeft.addEventListener( MouseEvent.MOUSE_DOWN, function( event:Event ) 
        {
            dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_DOWN, "a" ) );
        } );
        
        touchButtonLeft.addEventListener( MouseEvent.MOUSE_UP, function( event:Event ) 
        {
            dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_UP, "a" ) );
        } );
		
		touchButtonLeft.addEventListener( MouseEvent.MOUSE_OUT, function( event:Event ) 
        {
            dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_UP, "a" ) );
        } );
    }
        
    public function rightButton() : Void 
    {
        touchButtonRight = factory.getActorFromStringForForeground( "assets/img/buttons/button_left_right.png", 206.50, 515.50 );
        touchButtonRight.buttonMode = true;
           
        touchButtonRight.addEventListener( MouseEvent.MOUSE_DOWN, function( event:Event ) 
		{
            dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_DOWN, "d" ) );
        } );
       
        touchButtonRight.addEventListener( MouseEvent.MOUSE_UP, function( event:Event ) 
        {
            dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_UP, "d" ) );
        } );
			
		touchButtonRight.addEventListener( MouseEvent.MOUSE_OUT, function( event:Event ) 
        {
            dispatchEvent( new GamepadEvent( KeyboardEvent.KEY_UP, "d" ) );
        } );
    }       
}