package game;

import engine.render.Actor;
import engine.SceneManager;
import engine.StateMachine;

import nme.Assets;
import nme.display.Stage;
import nme.media.Sound;
import nme.geom.Point;


/**
 * Handles the collision locic for the player/user
*/


class PlayerCollision
{
	public var player:Actor;
	public var playerController:PlayerController;
	public var accessGranted:Bool;
	public var sceneMgr:SceneManager;
	
	private var actorArray:Array<Actor>;
	private var iterator:Iterator<Actor>;
	private var scenario:StateMachine;
	private var currentLevel:Int = 0;
	private var highestLevelWon:Int = -1;
	private var accessGrantedSound:Sound;
	private var itBurnsSound:Sound;
	private var sizzleSound:Sound;
	private var warpSound:Sound;
	private var newPosition:Point;
	
	
	public function new( player:Actor, stage:Stage, sceneMgr:SceneManager, scenario:StateMachine ) : Void
	{
		this.player = player;
		this.sceneMgr = sceneMgr;
		this.scenario = scenario;
	}
	

	public function getColliders( oldPosition:Point ) : Void
	{
		actorArray = sceneMgr.rectSceneQuery( player.actorBoundingBox );
		
		iterator  = actorArray.iterator();
		while ( iterator.hasNext() )
		{
			var actor:Actor = iterator.next();
			if ( actor != player )
			{
				if ( actor.collisionType == Actor.PICKUP )				//grabs access card to continue to lower levels
				{
					sceneMgr.removeFromGameLayer(actor);
					cast(scenario.activeState, LevelLayout).cardPosition.x = -10000;
					accessGranted = true;
					highestLevelWon = (highestLevelWon < currentLevel) ? currentLevel : highestLevelWon;
					accessGrantedSound = Assets.getSound("assets/audio/accessgranted.mp3");
					accessGrantedSound.play();
					//trace("Access Granted");
				}
				
				else if ( actor.collisionType == Actor.HAZARD )			//resets player position
				{
					//reset player to starting positiion
					player.setActorPos( cast( scenario.activeState, LevelLayout ).playerPosition.x, cast( scenario.activeState, LevelLayout ).playerPosition.y );
					player.actorBoundingBox.setTo( player.x, player.y, player.actorBoundingBox.boxWidth, player.actorBoundingBox.boxHeight );
					itBurnsSound = Assets.getSound("assets/audio/damnitBurns.mp3");
					itBurnsSound.play();
					sizzleSound = Assets.getSound("assets/audio/sizzle.mp3");
					sizzleSound.play();
					//trace("Hazard");
					
					if (accessGranted)
					{
						// reset player to card position
						player.setActorPos(cast(scenario.activeState, LevelLayout).oldCardPosition.x, cast(scenario.activeState, LevelLayout).oldCardPosition.y); 
						player.actorBoundingBox.setTo(player.x, player.y, player.actorBoundingBox.boxWidth, player.actorBoundingBox.boxHeight);
					}
					
					break;
				}
				/*
				else if ( actor.collisionType == Actor.PORTAL_UP )    	//enable to access previous floors
				{
						if ( currentLevel == 0 )
						{
							continue;
						}
						
						scenario.enterState( "BLevel" + ( --currentLevel ) );
						player.setActorPos( cast( scenario.activeState, LevelLayout ).exitEntrancePosition.x, cast( scenario.activeState, LevelLayout ).exitEntrancePosition.y );
						accessGranted = true;
						warpSound = Assets.getSound( "assets/audio/warp.mp3" );
						warpSound.play();
				}
				*/
				else if ( actor.collisionType == Actor.PORTAL_DOWN )	//access lower levels
				{
					if ( accessGranted )
					{
						scenario.enterState( "BLevel" + ( ++currentLevel ) );
						player.setActorPos( cast( scenario.activeState, LevelLayout ).playerPosition.x, cast( scenario.activeState, LevelLayout ).playerPosition.y );
						accessGranted = highestLevelWon >= currentLevel;
						warpSound = Assets.getSound( "assets/audio/warp.mp3" );
						warpSound.play();
					}
					else 
					{
						
					}
				}
				
				if ( actor.collisionType == Actor.OBSTACLE )			//collision against tables and walls
				{
					newPosition = player.getActorPos();
					player.setActorPos( oldPosition.x, oldPosition.y );
				}
			}
		}
	}
}