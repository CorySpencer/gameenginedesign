package game;

import nme.events.Event;

/**
 * Info needed to handles navigating through menues
*/


class MenuNavEvent extends Event
{
	public inline static var START_TYPE:String = "start";
	public inline static var INSTRUCTION_TYPE:String = "instr";
	public inline static var TITLE_TYPE:String = "title";
	
	public function new( type:String, cancelable:Bool=false ) 
	{
		super( type, cancelable );
	}
}