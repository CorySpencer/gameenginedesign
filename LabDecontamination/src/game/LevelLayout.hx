package game;

import engine.interfaces.IState;
import engine.render.Actor;
import engine.render.ActorFactory;
import engine.SceneManager;

import nme.geom.Point;


class LevelLayout implements IState
{	
	public var playerPosition:Point;
	public var cardPosition:Point;
	public var oldCardPosition:Point;
	public var exitEntrancePosition:Point;
	
	private var factory:ActorFactory;
	private var sceneMgr:SceneManager;
	private var enterancePortalPosition:Point;
	private var exitPortalPosition:Point;
	private var superComputerPosition:Point;
	private var superComputerBitmap:String;
	private var horizontalTableBitmap:String;
	private var verticalTableBitmap:String;
	private var chemicalBitmap:String;
	private var entranceBitmap:String;
	private var exitBitmap:String;
	private var chemicalPositions:Array<Point>;
	private var verticalTablePositions:Array<Point>;
	private var horizontalTablePositions:Array<Point>;
	private var superComputerPositions:Array<Point>;
	private var actorTeardownArray:Array<Actor>;
	private static inline var WALL_NORTH:String = "assets/img/walls/wall_north.png";
	private static inline var WALL_SOUTH:String = "assets/img/walls/wall_south.png";
	private static inline var WALL_EAST:String = "assets/img/walls/wall_east.png";
	private static inline var WALL_WEST:String = "assets/img/walls/wall_west.png";
	private static inline var CARD_BITMAP:String = "assets/img/levelAssets/swipecard.png";
	private static inline var CHEMICAL_BITMAP:String = "assets/img/levelAssets/chemical1.png";
	private static inline var VERTICAL_TABLE_BITMAP:String = "assets/img/levelAssets/table_vertical.png";		
	private static inline var HORIZONTAL_TABLE_BITMAP:String = "assets/img/levelAssets/table_horozontal.png";

	public function new( sceneMgr:SceneManager, factory:ActorFactory ) : Void
	{
		this.factory = factory;
		this.sceneMgr = sceneMgr;
		actorTeardownArray = new Array<Actor>();
	}
	
	public function getPlayerLevelPosition() : Point
	{
		return playerPosition;
	}
	
	public function initB0Level() : Void
	{	
		entranceBitmap = "assets/img/floorPanels/l_up.png";
		exitBitmap = "assets/img/floorPanels/b1_down.png";
		
		chemicalPositions = new Array<Point>();
		verticalTablePositions = new Array<Point>();
		horizontalTablePositions = new Array<Point>();
		superComputerPositions = new Array<Point>();
		
		horizontalTablePositions.push( new Point( 145.50, 167.50 ) );
		horizontalTablePositions.push( new Point( 266.50, 167.50 ) );
		horizontalTablePositions.push( new Point( 387.50, 167.50 ) );
		horizontalTablePositions.push( new Point( 507.50, 167.50 ) );
		horizontalTablePositions.push( new Point( 628.50, 167.50 ) );
		horizontalTablePositions.push( new Point( 743.50, 167.50 ) );
		horizontalTablePositions.push( new Point( 864.50, 167.50 ) );
		horizontalTablePositions.push( new Point( 982.50, 167.50 ) );
		
		horizontalTablePositions.push( new Point( 145.50, 478.50 ) );
		horizontalTablePositions.push( new Point( 266.50, 478.50 ) );
		horizontalTablePositions.push( new Point( 387.50, 478.50 ) );
		horizontalTablePositions.push( new Point( 507.50, 478.50 ) );
		horizontalTablePositions.push( new Point( 628.50, 478.50 ) );
		horizontalTablePositions.push( new Point( 743.50, 478.50 ) );
		horizontalTablePositions.push( new Point( 864.50, 478.50 ) );
		horizontalTablePositions.push( new Point( 982.50, 478.50 ) );

		playerPosition = new Point( 156.50, 329.50 );
        cardPosition = new Point( 567.50, 321.50 );
		enterancePortalPosition = new Point( -1000.50, 1000.50 );
		exitPortalPosition = new Point( 1013.50, 340.50 );
		exitEntrancePosition = new Point( 924.94, 342.09 );
	}
	
	public function initB1Level() : Void
	{	
		entranceBitmap = "assets/img/floorPanels/l_up.png";
		exitBitmap = "assets/img/floorPanels/b2_down.png";
		
		chemicalPositions = new Array<Point>();
		verticalTablePositions = new Array<Point>();
		horizontalTablePositions = new Array<Point>();
		superComputerPositions = new Array<Point>();
		
		horizontalTablePositions.push( new Point( 145.50, 75.50 ) );
        horizontalTablePositions.push( new Point( 266.50, 75.50 ) );
        horizontalTablePositions.push( new Point( 387.50, 75.50 ) );
		horizontalTablePositions.push( new Point( 629.50, 75.50 ) );
        horizontalTablePositions.push( new Point( 750.50, 75.50 ) );
        horizontalTablePositions.push( new Point( 871.50, 75.50 ) );
        horizontalTablePositions.push( new Point( 992.50, 75.50 ) );
        horizontalTablePositions.push( new Point( 424.50, 293.50 ) );
        horizontalTablePositions.push( new Point( 303.50, 293.50 ) );
        horizontalTablePositions.push( new Point( 145.50, 565.50 ) );
        horizontalTablePositions.push( new Point( 266.50, 565.50 ) );
        horizontalTablePositions.push( new Point( 387.50, 565.50 ) );
        horizontalTablePositions.push( new Point( 629.50, 565.50 ) );
        horizontalTablePositions.push( new Point( 750.50, 565.50 ) );
        horizontalTablePositions.push( new Point( 871.50, 565.50 ) );
        horizontalTablePositions.push( new Point( 992.50, 565.50 ) );
		
		verticalTablePositions.push( new Point( 206.50, 161.50 ) );
        verticalTablePositions.push( new Point( 206.50, 268.50 ) );
        verticalTablePositions.push( new Point( 206.50, 381.50 ) );
        verticalTablePositions.push( new Point( 507.50, 221.50 ) );
        verticalTablePositions.push( new Point( 507.50, 329.50 ) );
        verticalTablePositions.push( new Point( 507.50, 432.50 ) );
		verticalTablePositions.push( new Point( 656.50, 161.50 ) );
        verticalTablePositions.push( new Point( 656.50, 268.50 ) );
        verticalTablePositions.push( new Point( 656.50, 389.50 ) );
        verticalTablePositions.push( new Point( 837.50, 236.50 ) );
        verticalTablePositions.push( new Point( 837.50, 357.50 ) );
        verticalTablePositions.push( new Point( 837.50, 478.50 ) );
        verticalTablePositions.push( new Point( 1013.50, 160.50 ) );
        verticalTablePositions.push( new Point( 1013.50, 478.50 ) );
		
		playerPosition = new Point( 1013.50, 270.50 );
        cardPosition = new Point( 298.50, 215.50 );
		enterancePortalPosition = new Point( 1013.50, 340.50 );
		exitPortalPosition = new Point( 138.50, 150.50 );
		exitEntrancePosition = new Point( 135.94, 251.09 );
	}
	
	public function initB2Level() : Void
	{	
		entranceBitmap = "assets/img/floorPanels/b1_up.png";
		exitBitmap = "assets/img/floorPanels/b3_down.png";
		
		chemicalPositions = new Array<Point>();
		verticalTablePositions = new Array<Point>();
		horizontalTablePositions = new Array<Point>();
		superComputerPositions = new Array<Point>();
		
		horizontalTablePositions.push( new Point( 146.50, 398.50 ) );
        horizontalTablePositions.push( new Point( 266.50, 398.50 ) );
        horizontalTablePositions.push( new Point( 387.50, 398.50 ) );
		horizontalTablePositions.push( new Point( 654.50, 159.50 ) ); 
        horizontalTablePositions.push( new Point( 775.50, 159.50 ) );
        horizontalTablePositions.push( new Point( 896.50, 159.50 ) );
        horizontalTablePositions.push( new Point( 836.50, 515.50 ) );
        horizontalTablePositions.push( new Point( 715.50, 515.50 ) );
        horizontalTablePositions.push( new Point( 775.50, 286.50 ) );
        horizontalTablePositions.push( new Point( 309.50, 497.50 ) );
		
		verticalTablePositions.push( new Point( 214.50, 108.50 ) );
        verticalTablePositions.push( new Point( 214.50, 237.50 ) );
        verticalTablePositions.push( new Point( 413.50, 311.50 ) );
        verticalTablePositions.push( new Point( 413.50, 193.50 ) );
        verticalTablePositions.push( new Point( 567.50, 193.50 ) );
        verticalTablePositions.push( new Point( 567.50, 320.50 ) );
		verticalTablePositions.push( new Point( 567.50, 441.50 ) );
        verticalTablePositions.push( new Point( 930.50, 236.50 ) );
        verticalTablePositions.push( new Point( 930.50, 354.50 ) );
        verticalTablePositions.push( new Point( 930.50, 475.50 ) );
        verticalTablePositions.push( new Point( 689.50, 432.50 ) );
        verticalTablePositions.push( new Point( 689.50, 320.50 ) );
        verticalTablePositions.push( new Point( 222.50, 531.50 ) );
        verticalTablePositions.push( new Point( 396.50, 531.50 ) );
		
		chemicalPositions.push( new Point( 567.50, 527.50 ) );
		chemicalPositions.push( new Point( 466.00, 339.00 ) );
		chemicalPositions.push( new Point( 975.50, 212.00 ) );
		chemicalPositions.push( new Point( 1031.50, 573.50 ) );
		chemicalPositions.push( new Point( 466.50, 527.00 ) );
		chemicalPositions.push( new Point( 878.50, 466.00 ) );
		chemicalPositions.push( new Point( 317.50, 353.00 ) );
		chemicalPositions.push( new Point( 514.50, 573.00 ) );
		chemicalPositions.push( new Point( 317.50, 100.00 ) );
		chemicalPositions.push( new Point( 745.50, 339.00 ) );
		chemicalPositions.push( new Point( 975.50, 391.00 ) );
		

		playerPosition = new Point( 138.50, 208.50 );
        cardPosition = new Point( 763.94, 454.09 );
		oldCardPosition = new Point( 763.94, 454.09 );
		enterancePortalPosition = new Point( 138.50, 108.50 );
		exitPortalPosition = new Point( 138.50, 539.50 );
		exitEntrancePosition = new Point( 140.94, 463.09 );	
	}
	
	public function initB3Level() : Void
	{	
		entranceBitmap = "assets/img/floorPanels/b2_up.png";
		exitBitmap = "assets/img/floorPanels/b4_down.png";
		
		chemicalPositions = new Array<Point>();
		verticalTablePositions = new Array<Point>();
		horizontalTablePositions = new Array<Point>();
		superComputerPositions = new Array<Point>();
		
		horizontalTablePositions.push( new Point( 263.50, 566.50 ) );
        horizontalTablePositions.push( new Point( 384.50, 566.50 ) );
        horizontalTablePositions.push( new Point( 505.50, 566.50 ) );
        horizontalTablePositions.push( new Point( 626.50, 566.50 ) );
        horizontalTablePositions.push( new Point( 747.50, 566.50 ) );
        horizontalTablePositions.push( new Point( 868.50, 566.50 ) );
		horizontalTablePositions.push( new Point( 989.50, 566.50 ) );
        horizontalTablePositions.push( new Point( 202.50, 169.50 ) );
        horizontalTablePositions.push( new Point( 323.50, 169.50 ) );
        horizontalTablePositions.push( new Point( 530.50, 75.50 ) );
        horizontalTablePositions.push( new Point( 815.50, 75.50 ) );
        horizontalTablePositions.push( new Point( 642.50, 316.50 ) );
        horizontalTablePositions.push( new Point( 642.50, 385.50 ) );
		
		verticalTablePositions.push( new Point( 112.50, 109.50 ) );
        verticalTablePositions.push( new Point( 112.50, 230.50 ) );
        verticalTablePositions.push( new Point( 112.50, 351.50 ) );
		verticalTablePositions.push( new Point( 902.50, 109.50 ) ); 
        verticalTablePositions.push( new Point( 902.50, 230.50 ) );
        verticalTablePositions.push( new Point( 902.50, 351.50 ) );
        verticalTablePositions.push( new Point( 262.50, 351.50 ) );
        verticalTablePositions.push( new Point( 410.50, 351.50 ) );
        verticalTablePositions.push( new Point( 555.50, 351.50 ) );
        verticalTablePositions.push( new Point( 729.50, 351.50 ) );
		verticalTablePositions.push( new Point( 676.50, 180.50 ) );
		
		chemicalPositions.push( new Point( 262.50, 445.50 ) );
		chemicalPositions.push( new Point( 263.00, 506.00 ) );
		chemicalPositions.push( new Point( 463.50, 320.00 ) );
		chemicalPositions.push( new Point( 505.50, 371.50 ) );
		chemicalPositions.push( new Point( 410.50, 241.00 ) );
		chemicalPositions.push( new Point( 409.50, 445.00 ) );
		chemicalPositions.push( new Point( 549.50, 506.00 ) );
		chemicalPositions.push( new Point( 172.50, 230.00 ) );
		chemicalPositions.push( new Point( 676.50, 272.00 ) );
		chemicalPositions.push( new Point( 500.50, 139.00 ) );
		chemicalPositions.push( new Point( 563.50, 203.00 ) );
		chemicalPositions.push( new Point( 789.50, 192.00 ) );
		chemicalPositions.push( new Point( 857.50, 378.00 ) );
		chemicalPositions.push( new Point( 902.50, 520.00 ) );
		chemicalPositions.push( new Point( 957.50, 291.00 ) );
		chemicalPositions.push( new Point( 1031.50, 445.00 ) );
		
		playerPosition = new Point( 138.94, 446.09 );
        cardPosition = new Point( 190.94, 95.09 );
		oldCardPosition = new Point( 190.94, 95.09 );
		enterancePortalPosition = new Point( 138.50, 539.50 );
		exitPortalPosition = new Point( 991.50, 100.50 );
		exitEntrancePosition = new Point( 991.94, 192.09 );	
	}
	
	public function initB4Level() : Void
	{	
		entranceBitmap = "assets/img/floorPanels/b3_up.png";
		exitBitmap = "assets/img/floorPanels/b5_down.png";
		
		chemicalPositions = new Array<Point>();
		verticalTablePositions = new Array<Point>();
		horizontalTablePositions = new Array<Point>();	
		superComputerPositions = new Array<Point>();
		
		verticalTablePositions.push( new Point( 233.50, 194.50 ) );
        verticalTablePositions.push( new Point( 233.50, 320.50 ) );
        verticalTablePositions.push( new Point( 233.50, 441.50 ) );
		verticalTablePositions.push( new Point( 620.50, 109.50 ) ); 
        verticalTablePositions.push( new Point( 902.50, 109.50 ) );
        verticalTablePositions.push( new Point( 902.50, 109.50 ) );
        verticalTablePositions.push( new Point( 468.50, 233.50 ) );
        verticalTablePositions.push( new Point( 768.50, 233.50 ) );
        verticalTablePositions.push( new Point( 380.50, 474.50 ) );
        verticalTablePositions.push( new Point( 520.50, 474.50 ) );
		verticalTablePositions.push( new Point( 661.50, 474.50 ) );
		verticalTablePositions.push( new Point( 802.50, 474.50 ) );
		verticalTablePositions.push( new Point( 949.50, 474.50 ) );
		
		horizontalTablePositions.push( new Point( 146.50, 320.50 ) );
        horizontalTablePositions.push( new Point( 320.50, 320.50 ) );
        horizontalTablePositions.push( new Point( 441.50, 320.50 ) );
        horizontalTablePositions.push( new Point( 647.50, 320.50 ) );
        horizontalTablePositions.push( new Point( 768.50, 320.50 ) );
        horizontalTablePositions.push( new Point( 889.50, 320.50 ) );
		
		chemicalPositions.push( new Point( 520.50, 320.50 ) );
		chemicalPositions.push( new Point( 352.00, 100.00 ) );
		chemicalPositions.push( new Point( 950.50, 381.00 ) );
		chemicalPositions.push( new Point( 857.50, 474.50 ) );
		chemicalPositions.push( new Point( 568.50, 320.00 ) );
		chemicalPositions.push( new Point( 352.50, 233.00 ) );
		chemicalPositions.push( new Point( 802.50, 563.00 ) );
		chemicalPositions.push( new Point( 661.50, 377.00 ) );
		
		chemicalPositions.push( new Point( 750.50, 474.00 ) );
		chemicalPositions.push( new Point( 380.50, 563.00 ) );
		chemicalPositions.push( new Point( 425.50, 474.00 ) );
		chemicalPositions.push( new Point( 612.50, 475.00 ) );
		chemicalPositions.push( new Point( 290.50, 377.00 ) );
		chemicalPositions.push( new Point( 616.50, 211.00 ) );
		chemicalPositions.push( new Point( 568.50, 110.00 ) );
		
		playerPosition = new Point( 993.94, 189.09 );
        cardPosition = new Point( 140.94, 379.09 );
		oldCardPosition = new Point( 140.94, 379.09 );
		enterancePortalPosition = new Point( 991.50, 100.50 );
		exitPortalPosition = new Point( 146.50, 246.50 );
		exitEntrancePosition = new Point( 143.94, 172.09 );	
	}
	
	public function initB5Level() : Void
	{	
		entranceBitmap = "assets/img/floorPanels/b4_up.png";
		exitBitmap = "assets/img/floorPanels/b6_down.png";
		
		chemicalPositions = new Array<Point>();
		verticalTablePositions = new Array<Point>();
		horizontalTablePositions = new Array<Point>();
		superComputerPositions = new Array<Point>();
		
		verticalTablePositions.push( new Point( 234.50, 246.50 ) );
        verticalTablePositions.push( new Point( 904.50, 246.50 ) );
		verticalTablePositions.push( new Point( 234.50, 367.50 ) );
        verticalTablePositions.push( new Point( 904.50, 367.50 ) );
		
		horizontalTablePositions.push( new Point( 817.50, 166.50 ) );
        horizontalTablePositions.push( new Point( 697.50, 166.50 ) );
        horizontalTablePositions.push( new Point( 576.50, 166.50 ) );
        horizontalTablePositions.push( new Point( 455.50, 166.50 ) );
        horizontalTablePositions.push( new Point( 989.50, 166.50 ) );
        horizontalTablePositions.push( new Point( 147.50, 166.50 ) );
		horizontalTablePositions.push( new Point( 268.50, 562.50 ) );
        horizontalTablePositions.push( new Point( 389.50, 562.50 ) );
        horizontalTablePositions.push( new Point( 507.50, 562.50 ) );
        horizontalTablePositions.push( new Point( 628.50, 562.50 ) );
        horizontalTablePositions.push( new Point( 749.50, 562.50 ) );
        horizontalTablePositions.push( new Point( 870.50, 562.50 ) );
		horizontalTablePositions.push( new Point( 878.50, 454.50 ) );
        horizontalTablePositions.push( new Point( 757.50, 454.50 ) );
        horizontalTablePositions.push( new Point( 636.50, 454.50 ) );
        horizontalTablePositions.push( new Point( 515.50, 454.50 ) );
        horizontalTablePositions.push( new Point( 394.50, 454.50 ) );
		horizontalTablePositions.push( new Point( 321.50, 307.50 ) );
        horizontalTablePositions.push( new Point( 442.50, 307.50 ) );
        horizontalTablePositions.push( new Point( 563.50, 307.50 ) );
        horizontalTablePositions.push( new Point( 684.50, 307.50 ) );
		
		chemicalPositions.push( new Point( 111.50, 562.50 ) );
		chemicalPositions.push( new Point( 175.00, 562.00 ) );
		chemicalPositions.push( new Point( 1010.50, 562.00 ) );
		chemicalPositions.push( new Point( 957.50, 562.50 ) );
		chemicalPositions.push( new Point( 310.50, 68.00 ) );
		chemicalPositions.push( new Point( 460.50, 212.00 ) );
		chemicalPositions.push( new Point( 234.50, 455.00 ) );
		chemicalPositions.push( new Point( 234.50, 159.00 ) );
		chemicalPositions.push( new Point( 904.50, 159.00 ) );
		chemicalPositions.push( new Point( 307.50, 159.00 ) );
		chemicalPositions.push( new Point( 156.50, 89.00 ) );
		chemicalPositions.push( new Point( 586.50, 67.00 ) );
		chemicalPositions.push( new Point( 1026.50, 455.50 ) );
		chemicalPositions.push( new Point( 286.00, 353.00 ) );
		chemicalPositions.push( new Point( 400.50, 409.00 ) );
		chemicalPositions.push( new Point( 521.50, 353.50 ) );
		chemicalPositions.push( new Point( 636.50, 353.00 ) );
		chemicalPositions.push( new Point( 707.50, 67.00 ) );
		chemicalPositions.push( new Point( 616.50, 211.00 ) );
		chemicalPositions.push( new Point( 799.50, 409.00 ) );
		chemicalPositions.push( new Point( 851.50, 307.00 ) );
		chemicalPositions.push( new Point( 716.50, 261.00 ) );
		chemicalPositions.push( new Point( 370.50, 261.00 ) );
		chemicalPositions.push( new Point( 832.50, 124.00 ) );
		
		playerPosition = new Point( 152.94, 320.09 );
        cardPosition = new Point( 978.94, 86.09 );
		oldCardPosition = new Point( 978.94, 86.09 );
		enterancePortalPosition = new Point( 146.50, 246.50 );
		exitPortalPosition = new Point( 991.50, 246.50 );
		exitEntrancePosition = new Point( 991.94, 309.09 );	
	}
	
	public function initB6Level() : Void
	{	
		entranceBitmap = "assets/img/floorPanels/b5_up.png";
		exitBitmap = "assets/img/floorPanels/b7_down.png";
		
		chemicalPositions = new Array<Point>();
		verticalTablePositions = new Array<Point>();
		horizontalTablePositions = new Array<Point>();
		superComputerPositions = new Array<Point>();
		
		verticalTablePositions.push( new Point( 112.50, 528.50 ) );
        verticalTablePositions.push( new Point( 570.50, 467.50 ) );
		verticalTablePositions.push( new Point( 294.50, 108.50 ) );
        verticalTablePositions.push( new Point( 537.50, 108.50 ) );
		verticalTablePositions.push( new Point( 779.50, 108.50 ) );
        verticalTablePositions.push( new Point( 415.50, 233.50 ) );
		verticalTablePositions.push( new Point( 658.50, 233.50 ) );
        verticalTablePositions.push( new Point( 900.50, 233.50 ) );
		
		horizontalTablePositions.push( new Point( 146.50, 165.50 ) );
        horizontalTablePositions.push( new Point( 268.50, 320.50 ) );
        horizontalTablePositions.push( new Point( 389.50, 320.50 ) );
        horizontalTablePositions.push( new Point( 510.50, 320.50 ) );
        horizontalTablePositions.push( new Point( 631.50, 320.50 ) );
        horizontalTablePositions.push( new Point( 752.50, 320.50 ) );
		horizontalTablePositions.push( new Point( 873.50, 320.50 ) );
        horizontalTablePositions.push( new Point( 994.50, 320.50 ) );
      
		chemicalPositions.push( new Point( 415.50, 680.50 ) );
		chemicalPositions.push( new Point( 658.00, 680.00 ) );
		chemicalPositions.push( new Point( 900.50, 680.00 ) );
		chemicalPositions.push( new Point( 110.50, 439.50 ) );
		chemicalPositions.push( new Point( 175.50, 439.00 ) );
		chemicalPositions.push( new Point( 312.50, 439.00 ) );
		chemicalPositions.push( new Point( 423.50, 439.00 ) );
		chemicalPositions.push( new Point( 696.50, 439.00 ) );
		chemicalPositions.push( new Point( 833.50, 439.00 ) );
		chemicalPositions.push( new Point( 944.50, 439.00 ) );
		chemicalPositions.push( new Point( 231.50, 275.00 ) );
		chemicalPositions.push( new Point( 533.50, 275.00 ) );
		chemicalPositions.push( new Point( 779.50, 275.50 ) );
		chemicalPositions.push( new Point( 175.00, 498.00 ) );
		chemicalPositions.push( new Point( 423.50, 498.00 ) );
		chemicalPositions.push( new Point( 696.50, 498.50 ) );
		chemicalPositions.push( new Point( 944.50, 498.00 ) );
		chemicalPositions.push( new Point( 104.50, 320.00 ) );
		chemicalPositions.push( new Point( 194.50, 320.00 ) );
		chemicalPositions.push( new Point( 231.50, 173.00 ) );
		chemicalPositions.push( new Point( 176.50, 562.00 ) );
		chemicalPositions.push( new Point( 249.50, 562.00 ) );
		chemicalPositions.push( new Point( 310.50, 562.00 ) );
		chemicalPositions.push( new Point( 370.50, 562.00 ) );
		chemicalPositions.push( new Point( 423.50, 562.00 ) );
		chemicalPositions.push( new Point( 697.50, 562.00 ) );
		chemicalPositions.push( new Point( 770.50, 562.00 ) );
		chemicalPositions.push( new Point( 312.50, 382.00 ) );
		chemicalPositions.push( new Point( 570.50, 382.00 ) );
		chemicalPositions.push( new Point( 833.50, 382.00 ) );
		chemicalPositions.push( new Point( 831.50, 562.00 ) );
		chemicalPositions.push( new Point( 891.50, 562.00 ) );
		chemicalPositions.push( new Point( 944.50, 562.00 ) );
		
		playerPosition = new Point( 989.94, 151.09 );
        cardPosition = new Point( 1006.94, 550.09 );
		oldCardPosition = new Point( 1006.94, 550.09 );
		enterancePortalPosition = new Point( 991.50, 246.50 );
		exitPortalPosition = new Point( 144.50, 246.50 );
		exitEntrancePosition = new Point( 240.94, 246.09 );	
	}
	
	public function initB7Level() : Void
	{	
		entranceBitmap = "assets/img/floorPanels/b6_up.png";
		exitBitmap = "assets/img/floorPanels/b8_down.png";
		
		chemicalPositions = new Array<Point>();
		verticalTablePositions = new Array<Point>();
		horizontalTablePositions = new Array<Point>();
		superComputerPositions = new Array<Point>();
		
		verticalTablePositions.push( new Point( 337.50, 267.50 ) );
        verticalTablePositions.push( new Point( 337.50, 388.50 ) );
		verticalTablePositions.push( new Point( 1021.50, 119.50 ) );
        verticalTablePositions.push( new Point( 1021.50, 245.50 ) );
		verticalTablePositions.push( new Point( 1021.50, 376.50 ) );
		
		horizontalTablePositions.push( new Point( 250.50, 181.50 ) );
        horizontalTablePositions.push( new Point( 371.50, 181.50 ) );
        horizontalTablePositions.push( new Point( 492.50, 181.50 ) );
        horizontalTablePositions.push( new Point( 613.50, 181.50 ) );
        horizontalTablePositions.push( new Point( 734.50, 181.50 ) );
        horizontalTablePositions.push( new Point( 855.50, 181.50 ) );
		horizontalTablePositions.push( new Point( 510.50, 468.50 ) );
        horizontalTablePositions.push( new Point( 631.50, 468.50 ) );
        horizontalTablePositions.push( new Point( 752.50, 468.50 ) );
        horizontalTablePositions.push( new Point( 873.50, 468.50 ) );
        horizontalTablePositions.push( new Point( 994.50, 468.50 ) );
		
		chemicalPositions.push( new Point( 109.50, 181.50 ) );
		chemicalPositions.push( new Point( 160.00, 181.00 ) );
		chemicalPositions.push( new Point( 107.50, 309.00 ) );
		chemicalPositions.push( new Point( 157.50, 309.50 ) );
		chemicalPositions.push( new Point( 208.50, 309.00 ) );
		chemicalPositions.push( new Point( 179.50, 430.00 ) );
		chemicalPositions.push( new Point( 229.50, 430.00 ) );
		chemicalPositions.push( new Point( 280.50, 430.00 ) );
		chemicalPositions.push( new Point( 109.50, 572.00 ) );
		chemicalPositions.push( new Point( 159.50, 572.00 ) );
		chemicalPositions.push( new Point( 210.50, 572.00 ) );
		chemicalPositions.push( new Point( 337.50, 468.00 ) );
		chemicalPositions.push( new Point( 337.50, 509.50 ) );
		chemicalPositions.push( new Point( 474.50, 283.00 ) );
		chemicalPositions.push( new Point( 474.50, 376.00 ) );
		chemicalPositions.push( new Point( 474.50, 330.00 ) );
		chemicalPositions.push( new Point( 474.50, 422.00 ) );
		chemicalPositions.push( new Point( 586.50, 226.00 ) );
		chemicalPositions.push( new Point( 586.50, 273.00 ) );
		chemicalPositions.push( new Point( 586.50, 319.00 ) );
		chemicalPositions.push( new Point( 586.50, 365.00 ) );
		chemicalPositions.push( new Point( 691.50, 227.00 ) );
		chemicalPositions.push( new Point( 691.50, 331.00 ) );
		chemicalPositions.push( new Point( 691.50, 377.00 ) );
		chemicalPositions.push( new Point( 691.50, 423.00 ) );
		chemicalPositions.push( new Point( 803.50, 227.00 ) );
		chemicalPositions.push( new Point( 803.50, 274.00 ) );
		chemicalPositions.push( new Point( 803.50, 320.00 ) );
		chemicalPositions.push( new Point( 803.50, 422.00 ) );
		chemicalPositions.push( new Point( 854.50, 227.00 ) );
		chemicalPositions.push( new Point( 854.50, 274.00 ) );
		chemicalPositions.push( new Point( 854.50, 422.00 ) );
		chemicalPositions.push( new Point( 905.50, 227.00 ) );
		chemicalPositions.push( new Point( 905.50, 377.00 ) );
		chemicalPositions.push( new Point( 905.50, 422.00 ) );
		chemicalPositions.push( new Point( 954.50, 331.00 ) );
		chemicalPositions.push( new Point( 954.50, 377.00 ) );
		chemicalPositions.push( new Point( 954.50, 422.00 ) );
		chemicalPositions.push( new Point( 221.50, 135.00 ) );
		chemicalPositions.push( new Point( 459.50, 135.00 ) );
		chemicalPositions.push( new Point( 689.50, 135.00 ) );
		chemicalPositions.push( new Point( 356.50, 68.00 ) );
		chemicalPositions.push( new Point( 586.50, 68.00 ) );
		chemicalPositions.push( new Point( 831.50, 68.00 ) );
		
		playerPosition = new Point( 232.94, 246.09 );
        cardPosition = new Point( 143.94, 100.09 );
		oldCardPosition = new Point( 143.94, 100.09 );
		enterancePortalPosition = new Point( 144.50, 246.50 );
		exitPortalPosition = new Point( 994.50, 538.50 );
		exitEntrancePosition = new Point( 906.94, 538.09 );
	}
	
	public function initB8Level() : Void
	{	
		entranceBitmap = "assets/img/floorPanels/b7_up.png";
		exitBitmap = "assets/img/floorPanels/b7_down.png";
		superComputerBitmap = "assets/img/levelAssets/supercomputer.png";
		
		chemicalPositions = new Array<Point>();
		verticalTablePositions = new Array<Point>();
		horizontalTablePositions = new Array<Point>();
		superComputerPositions = new Array<Point>();
		
		verticalTablePositions.push( new Point( 112.50, 160.50 ) );
        verticalTablePositions.push( new Point( 112.50, 320.50 ) );
		verticalTablePositions.push( new Point( 112.50, 478.50 ) );
        verticalTablePositions.push( new Point( 1021.50, 161.50 ) );
		verticalTablePositions.push( new Point( 1021.50, 321.50 ) );
		
		horizontalTablePositions.push( new Point( 146.50, 73.50 ) );
        horizontalTablePositions.push( new Point( 267.50, 73.50 ) );
        horizontalTablePositions.push( new Point( 388.50, 73.50 ) );
        horizontalTablePositions.push( new Point( 749.50, 73.50 ) );
        horizontalTablePositions.push( new Point( 870.50, 73.50 ) );
        horizontalTablePositions.push( new Point( 989.50, 73.50 ) );
		horizontalTablePositions.push( new Point( 146.50, 565.50 ) );
        horizontalTablePositions.push( new Point( 267.50, 565.50 ) );
        horizontalTablePositions.push( new Point( 388.50, 565.50 ) );
        horizontalTablePositions.push( new Point( 506.50, 565.50 ) );
        horizontalTablePositions.push( new Point( 627.50, 565.50 ) );
        horizontalTablePositions.push( new Point( 748.50, 565.50 ) );
		horizontalTablePositions.push( new Point( 869.50, 565.50 ) );
		
		chemicalPositions.push( new Point( 479.50, 70.50 ) );
		chemicalPositions.push( new Point( 664.00, 70.00 ) );
		chemicalPositions.push( new Point( 664.50, 120.00 ) );
		chemicalPositions.push( new Point( 717.50, 120.50 ) );
		chemicalPositions.push( new Point( 767.50, 120.00 ) );
		chemicalPositions.push( new Point( 818.50, 120.00 ) );
		chemicalPositions.push( new Point( 224.50, 168.00 ) );
		chemicalPositions.push( new Point( 275.50, 168.00 ) );
		chemicalPositions.push( new Point( 327.50, 168.00 ) );
		chemicalPositions.push( new Point( 379.50, 168.00 ) );
		chemicalPositions.push( new Point( 430.50, 168.00 ) );
		chemicalPositions.push( new Point( 479.50, 168.00 ) );
		chemicalPositions.push( new Point( 529.50, 168.50 ) );
		chemicalPositions.push( new Point( 575.50, 168.00 ) );
		chemicalPositions.push( new Point( 621.50, 168.00 ) );
		chemicalPositions.push( new Point( 664.50, 168.00 ) );
		chemicalPositions.push( new Point( 717.50, 168.00 ) );
		chemicalPositions.push( new Point( 767.50, 168.00 ) );
		chemicalPositions.push( new Point( 224.50, 216.00 ) );
		chemicalPositions.push( new Point( 275.50, 216.00 ) );
		chemicalPositions.push( new Point( 479.50, 216.00 ) );
		chemicalPositions.push( new Point( 767.50, 216.00 ) );
		chemicalPositions.push( new Point( 901.50, 216.00 ) );
		chemicalPositions.push( new Point( 224.50, 265.00 ) );
		chemicalPositions.push( new Point( 379.50, 265.00 ) );
		chemicalPositions.push( new Point( 479.50, 265.00 ) );
		chemicalPositions.push( new Point( 579.50, 265.00 ) );
		chemicalPositions.push( new Point( 621.50, 265.00 ) );
		chemicalPositions.push( new Point( 664.50, 265.00 ) );
		chemicalPositions.push( new Point( 767.50, 265.00 ) );
		chemicalPositions.push( new Point( 856.50, 265.00 ) );
		chemicalPositions.push( new Point( 901.50, 265.00 ) );
		chemicalPositions.push( new Point( 224.50, 312.00 ) );
		chemicalPositions.push( new Point( 327.50, 312.00 ) );
		chemicalPositions.push( new Point( 379.50, 312.00 ) );
		chemicalPositions.push( new Point( 579.50, 312.00 ) );
		chemicalPositions.push( new Point( 767.50, 312.00 ) );
		chemicalPositions.push( new Point( 901.50, 312.00 ) );
		chemicalPositions.push( new Point( 224.50, 362.00 ) );
		chemicalPositions.push( new Point( 379.50, 362.00 ) );
		chemicalPositions.push( new Point( 430.50, 362.00 ) );
		chemicalPositions.push( new Point( 479.50, 362.00 ) );
		chemicalPositions.push( new Point( 528.50, 362.00 ) );
		chemicalPositions.push( new Point( 579.50, 362.00 ) );	
		chemicalPositions.push( new Point( 670.50, 362.00 ) );
		chemicalPositions.push( new Point( 718.50, 362.00 ) );
		chemicalPositions.push( new Point( 767.50, 362.00 ) );
		chemicalPositions.push( new Point( 808.50, 362.00 ) );
		chemicalPositions.push( new Point( 901.50, 362.00 ) );
		chemicalPositions.push( new Point( 224.50, 410.00 ) );
		chemicalPositions.push( new Point( 275.50, 410.00 ) );
		chemicalPositions.push( new Point( 579.50, 410.00 ) );
		chemicalPositions.push( new Point( 670.50, 410.00 ) );
		chemicalPositions.push( new Point( 901.50, 410.00 ) );
		chemicalPositions.push( new Point( 224.50, 460.00 ) );
		chemicalPositions.push( new Point( 275.50, 460.00 ) );
		chemicalPositions.push( new Point( 328.50, 460.00 ) );
		chemicalPositions.push( new Point( 379.50, 460.00 ) );
		chemicalPositions.push( new Point( 430.50, 460.00 ) );
		chemicalPositions.push( new Point( 479.50, 460.00 ) );
		chemicalPositions.push( new Point( 578.50, 460.00 ) );
		chemicalPositions.push( new Point( 767.50, 460.00 ) );
		chemicalPositions.push( new Point( 810.50, 460.00 ) );
		chemicalPositions.push( new Point( 856.50, 460.00 ) );
		chemicalPositions.push( new Point( 901.50, 460.00 ) );
		chemicalPositions.push( new Point( 579.50, 509.00 ) );
		chemicalPositions.push( new Point( 627.50, 509.00 ) );
		chemicalPositions.push( new Point( 670.50, 509.00 ) );
		chemicalPositions.push( new Point( 718.50, 509.00 ) );
		chemicalPositions.push( new Point( 767.50, 509.00 ) );
		chemicalPositions.push( new Point( 810.50, 509.00 ) );
		chemicalPositions.push( new Point( 856.50, 509.00 ) );
		chemicalPositions.push( new Point( 901.50, 509.00 ) );
		
		superComputerPositions.push( new Point( 579.35, 39.66 ) );
		
		playerPosition = new Point( 990.94, 445.09 );
        cardPosition = new Point( -1000.94, -1000.09 );
		enterancePortalPosition = new Point( 994.50, 538.50 );
		exitPortalPosition = new Point(  -1000.50, -1000.50 );
	}
	
	public function setup() : Void
	{	
		actorTeardownArray.push( factory.getActorFromStringForGameLayer( WALL_NORTH, 568, 24.5, Actor.OBSTACLE ) );
		actorTeardownArray.push( factory.getActorFromStringForGameLayer( WALL_SOUTH, 568.5, 615, Actor.OBSTACLE ) );
		actorTeardownArray.push( factory.getActorFromStringForGameLayer( WALL_EAST, 1093, 320, Actor.OBSTACLE ) );
		actorTeardownArray.push( factory.getActorFromStringForGameLayer( WALL_WEST, 43.5, 320.5, Actor.OBSTACLE ) );
		
		actorTeardownArray.push( factory.getActorFromStringForGameLayer( CARD_BITMAP, cardPosition.x, cardPosition.y, Actor.PICKUP ) );	
		actorTeardownArray.push( factory.getActorFromStringForGameLayer( exitBitmap, exitPortalPosition.x, exitPortalPosition.y, Actor.PORTAL_DOWN ) );
		actorTeardownArray.push( factory.getActorFromStringForGameLayer( entranceBitmap, enterancePortalPosition.x, enterancePortalPosition.y, Actor.PORTAL_UP ) );
		
		var iterator:Iterator<Point> = horizontalTablePositions.iterator();
		while ( iterator.hasNext() )
		{
			var point:Point = iterator.next();
			actorTeardownArray.push( factory.getActorFromStringForGameLayer( HORIZONTAL_TABLE_BITMAP, point.x, point.y, Actor.OBSTACLE ) ); 
		}
		
		var iterator:Iterator<Point> = verticalTablePositions.iterator();
		while ( iterator.hasNext() )
		{
			var point:Point = iterator.next();
			actorTeardownArray.push( factory.getActorFromStringForGameLayer( VERTICAL_TABLE_BITMAP, point.x, point.y, Actor.OBSTACLE ) );
		}
		
		var iterator:Iterator<Point> = chemicalPositions.iterator();
		while ( iterator.hasNext() )
		{
			var point:Point = iterator.next();
			actorTeardownArray.push( factory.getActorFromStringForGameLayer( CHEMICAL_BITMAP, point.x, point.y, Actor.HAZARD ) );
		}
		
		var iterator:Iterator<Point> = superComputerPositions.iterator();
		while ( iterator.hasNext() )
		{
			var point:Point = iterator.next();
			actorTeardownArray.push( factory.getActorFromStringForGameLayer( superComputerBitmap, point.x, point.y, Actor.PORTAL_DOWN ) );
		}	
	}
	
	public function teardown() : Void
	{
		var iterator:Iterator<Actor> = actorTeardownArray.iterator();
		
		while ( iterator.hasNext() )
		{
			var actor:Actor = iterator.next();
			sceneMgr.removeFromGameLayer( actor );
		}
	}
}