package game.states;

import engine.interfaces.IState;

import nme.Assets;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.display.Stage;
import nme.events.Event;
import nme.events.EventDispatcher;
import nme.events.MouseEvent;
import nme.media.Sound;
import nme.media.SoundChannel;

/**
 * Represents a title screen
*/


class TitleScreenState extends EventDispatcher, implements IState
{
	private var background:Bitmap;
	private var stage:Stage;
	private var startMenu:Sprite;
	private var startButton:Sprite;
	private var instructionButton:Sprite;
	private var title_sound:Sound;
	
	public function new( stage:Stage ) 
	{
		super();
		this.stage = stage;
		
		background = new Bitmap( Assets.getBitmapData( "assets/img/start_menu.png" ) );
		startMenu = new Sprite();
		startMenu.addChild( background );
		
		startButton = new Sprite();
		startButton.addChild( new Bitmap( Assets.getBitmapData( "assets/img/buttons/press_start.png" ) ) );
		startButton.buttonMode = true;
		
		startMenu.addChild( startButton );
		startButton.x = 1018.0 - (startButton.width / 2.0);
		startButton.y = 527.0 - (startButton.height / 2.0);
		
		instructionButton = new Sprite();
		instructionButton.addChild( new Bitmap( Assets.getBitmapData( "assets/img/buttons/instructions_button.png" ) ) );
		instructionButton.buttonMode = true;
		
		startMenu.addChild( instructionButton );
		instructionButton.x = 1018.0 - ( instructionButton.width / 2.0 );
		instructionButton.y = 595.0 - ( instructionButton.height / 2.0 );
	}
	
	private function handleStartButton( event:Event ) : Void
	{
		dispatchEvent( new MenuNavEvent( MenuNavEvent.START_TYPE ) );					//dispatch state change event to game screen
	}
	
	private function handleInstructionButton( event:Event ) : Void
	{
		dispatchEvent( new MenuNavEvent( MenuNavEvent.INSTRUCTION_TYPE ) );				//dispatch state change event to instruction scree
	}
	
	public function setup() : Void
	{
		stage.addChild( startMenu );
		startButton.addEventListener( MouseEvent.CLICK, handleStartButton );
		instructionButton.addEventListener( MouseEvent.CLICK, handleInstructionButton );
		title_sound = Assets.getSound("assets/audio/lab_d_intro.mp3");
		title_sound.play();
	}
	
	public function teardown() : Void
	{
		startButton.removeEventListener( MouseEvent.CLICK, handleStartButton );
		instructionButton.removeEventListener( MouseEvent.CLICK, handleInstructionButton );
		stage.removeChild( startMenu );
	}
}