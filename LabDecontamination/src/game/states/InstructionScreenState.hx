package game.states;

import engine.interfaces.IState;

import nme.Assets;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.display.Stage;
import nme.events.Event;
import nme.events.EventDispatcher;
import nme.events.MouseEvent;

/**
 * Represents a title screen
*/


class InstructionScreenState extends EventDispatcher, implements IState
{
	private var stage:Stage;
	private var menuSprite:Sprite;
	private var backButton:Sprite;
	private var background:Bitmap;

	public function new( stage:Stage ) 
	{
		super();
		this.stage = stage;
		background = new Bitmap( Assets.getBitmapData( "assets/img/instruction_screen.png" ) );
		menuSprite = new Sprite();
		menuSprite.addChild( background );
		
		backButton = new Sprite();
		backButton.addChild( new Bitmap( Assets.getBitmapData( "assets/img/buttons/back_button.png" ) ) );
		backButton.buttonMode = true;
		
		menuSprite.addChild( backButton );
		backButton.x = 131.0 - ( backButton.width / 2.0 );
		backButton.y = 597.0 - ( backButton.height / 2.0 );
	}
	
	private function handleBackButton( event:Event ) : Void
	{
		dispatchEvent( new MenuNavEvent( MenuNavEvent.TITLE_TYPE ) );			//dispatch state change event to title screen
	}
	
	public function setup() : Void
	{
		stage.addChild( menuSprite );
		backButton.addEventListener( MouseEvent.CLICK, handleBackButton );
	}
	
	public function teardown() : Void
	{
		backButton.removeEventListener( MouseEvent.CLICK, handleBackButton );
		stage.removeChild( menuSprite );
	}
}