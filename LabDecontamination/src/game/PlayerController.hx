package game;

import engine.render.Actor;
import engine.render.ActorFactory;
import engine.interfaces.ITickable;
import engine.GamepadEvent;
import engine.SceneManager;
import engine.StateMachine;
import engine.Rectangle;
import game.LevelLayout;
import game.TouchButtons;

import nme.Lib;
import nme.Assets;
import nme.display.Sprite;
import nme.display.Stage;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.events.TouchEvent;
import nme.events.KeyboardEvent;
import nme.geom.Point;
import nme.geom.Vector3D;

/**
 * Handles controller inputs
*/


class PlayerController implements ITickable
{
	public var player:Actor;
	public var accessGranted:Bool;
	public var sceneMgr:SceneManager;
	public var collisions:PlayerCollision;
	
	private var oldDirection:Point;
	private var walkUp:Bool;
    private var walkDown:Bool;
    private var walkLeft:Bool;
    private var walkRight:Bool;
	private var walkFlags:Hash<Bool>;
	private var scenario:StateMachine;
	private var touchUp:TouchButtons;
	private var position:Point;
	private var oldPosition:Point;
	private var directionX:Float;
	private var directionY:Float;
	private var key:Int;
	private var isKeyDown:Bool;
	private	var hashKey:String;
	private var lastDirMoving:String = "Right";
	private static inline var STEP:Int = 3; 
	
	public function new( player:Actor, stage:Stage, sceneMgr:SceneManager, scenario:StateMachine ) : Void
	{	
		oldDirection = new Point(STEP, 0.0);
		this.player = player;
		this.sceneMgr = sceneMgr;
		this.scenario = scenario;
		collisions = new PlayerCollision( player, stage, sceneMgr, scenario );
		walkFlags = new Hash<Bool>();
		walkFlags.set( "w", false );
		walkFlags.set( "a", false );
		walkFlags.set( "s", false );
		walkFlags.set( "d", false );
		
		stage.addEventListener( KeyboardEvent.KEY_DOWN, keyHandler );
        stage.addEventListener( KeyboardEvent.KEY_UP, keyHandler );
		sceneMgr.buttons.addEventListener( KeyboardEvent.KEY_DOWN, keyHandler );
		sceneMgr.buttons.addEventListener( KeyboardEvent.KEY_UP, keyHandler );
	}	

	public function onMouseMove( event:MouseEvent ) : Void
	{
		player.setActorPos( event.stageX, event.stageY );
	}
	
	public function onTouchMove( event:TouchEvent ) : Void
	{
		player.setActorPos( event.stageX, event.stageY );
	}
	
	
	public function walk() : Void
    {
        position = player.getActorPos();
        
        if ( walkFlags.get("w") )
        {
            position.y -= STEP;
        }
        
        if ( walkFlags.get("a") )
        {
            position.x -= STEP;
        }
        
        if ( walkFlags.get("s") )
        {
            position.y += STEP;
        }
          
        if ( walkFlags.get("d") )
        {
            position.x += STEP;
        }
		
		directionX = position.x - player.x;
		directionY = position.y - player.y;
		
		if ( ( oldDirection.x != directionX ) || ( oldDirection.y != directionY ) )
		{
			if ( ( directionX == directionY ) && ( directionX == 0.0 ) ) 					//For idle animations
			{
				player.runAnimation( "idle" + lastDirMoving );								// make sure states are called i.e. "idleRight"
			} 
			else if ( directionX > 0.0 ) 													// Left & right animation take priority over up & down
			{
				player.runAnimation( "walkRight" );
				lastDirMoving = "Right";
			} 
			else if ( directionX < 0.0 )
			{
				player.runAnimation( "walkLeft" );
				lastDirMoving = "Left";
				
			} 
			else if ( directionY > 0.0 ) 
			{
				player.runAnimation( "walkDown" );
				lastDirMoving = "Down";
			} 
			else if ( directionY < 0.0 )
			{
				player.runAnimation( "walkUp" );
				lastDirMoving = "Up";
			}
		}
		
		oldDirection.x = directionX;
		oldDirection.y = directionY;
		player.setActorPos( position.x, position.y );
    }
	
 
    public function keyHandler( event:KeyboardEvent ) : Void
    {
		key = event.keyCode;
		isKeyDown = event.type == KeyboardEvent.KEY_DOWN;
		
		if ( Std.is( event, GamepadEvent ) )
		{
			hashKey = cast( event, GamepadEvent ).direction;
		}
		else
		{
			hashKey = String.fromCharCode( event.charCode ).toLowerCase();
		}
		
		if ( walkFlags.exists( hashKey ) )
		{
			walkFlags.set( hashKey, isKeyDown );
			//trace ("Pressed");
		}
    }
	
	public function tick() : Void
	{
		oldPosition = player.getActorPos();
		walk();
		collisions.getColliders( oldPosition );
		
		if ( player.currentAnimation != null )
		{
			player.currentAnimation.tick();
		}
	}
}